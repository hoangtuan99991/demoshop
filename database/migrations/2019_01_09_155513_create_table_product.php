<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_product', function (Blueprint $table) {
            $table->increments('id');
            $table->text('product_name');
            $table->bigInteger('product_price');
            $table->string('product_image');
            $table->text('product_sub_image');
            $table->string('product_status');
            $table->text('product_description');
            $table->text('product_code');
            $table->integer('product_category', false, true);
            $table->timestamps();
            $table->foreign('product_category')->references('id')->on('table_category')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_product');
    }
}
