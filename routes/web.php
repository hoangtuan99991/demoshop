<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('shop.index');
});



Route::get('login', [ 'as' => 'login', 'uses' => 'Auth\LoginController@index']);
//admin page 
Route::prefix('admin')->group(function () {
    Route::get('', 'Admin\AdminController@index');
    Route::get('', 'Admin\AdminController@index');
    Route::get('/addproduct', 'Admin\AdminController@index');
    Route::post('/saveproduct', 'Admin\AdminController@saveproduct');

    Route::get('/addcategory', 'Admin\AdminController@addCategory');
    Route::post('/savecategory', 'Admin\AdminController@savecategory');

    Route::get('/product', 'Admin\AdminController@listProduct');
    Route::get('/delete-product', 'Admin\AdminController@deleteProduct');
});

Route::prefix('shop')->group(function () {
    Route::get('/', 'Shop\IndexController@index');
    Route::get('/product', 'Shop\IndexController@product');
    Route::get('/contact', 'Shop\IndexController@contact');
    Route::get('/product-detail', 'Shop\IndexController@getDetailProduct');
});