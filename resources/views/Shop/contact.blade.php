@extends('layout.shop')
@section('content')
<!-- Title Page -->
<section class="bg-title-page p-t-40 p-b-50 flex-col-c-m" style="background-image: url(images/heading-pages-06.jpg);">
	<h2 class="l-text2 t-center">
		About
	</h2>
</section>

<!-- content page -->
<section class="bgwhite p-t-66 p-b-38">
	<div class="container">
		<div class="row">
			<div class="col-md-4 p-b-30">
				<div class="hov-img-zoom">
					<img src="images/banner-14.jpg" alt="IMG-ABOUT">
				</div>
			</div>

			<div class="col-md-8 p-b-30">
				<h3 class="m-text26 p-t-15 p-b-16">
					Contact infomation
				</h3>

				<p class="p-b-28">
				Thanks for being a part of our family. We are more than happy to assist you with anything you may need. 
				</p>

				<div class="bo13 p-l-29 m-l-9 p-b-10">
					<p class="p-b-11">
					Our customer support information:
					</p>
					<p class="p-b-11">
					Email: info@viotonix.com
					</p>
					<p class="p-b-11">
					Telephone number:  +1 832 5643 549 
					</p>
					<p class="p-b-11">
					Address:  4483 Forest Drive Chantilly, VA 22021, United States
					</p>
				</div>

				<p class="p-b-28">
				We aim to respond within a few hours on all requests made, and never take longer than 24 hours to address anything you may need. Thanks and can’t wait to hear from you!
				</p>
			</div>
		</div>
	</div>
</section>
@endsection