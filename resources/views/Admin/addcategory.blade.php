@extends('layout.admin')
@section('content')
<div class="row">
    <!-- ============================================================== -->
    <!-- valifation types -->
    <!-- ============================================================== -->
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header">Create Product</h5>
            <div class="card-body">
                <form method="POST" enctype="multipart/form-data" action="/admin/savecategory">
                    @csrf
                    <div class="form-group row">
                        <label class="col-12 col-sm-3 col-form-label text-sm-right">Category name</label>
                        <div class="col-12 col-sm-8 col-lg-6">
                            <input type="text" required="" name="category_name"  class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-12 col-sm-3 col-form-label text-sm-right">Category code</label>
                        <div class="col-12 col-sm-8 col-lg-6">
                            <input type="text" required="" name="category_code" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-12 col-sm-3 col-form-label text-sm-right">Product description</label>
                        <div class="col-12 col-sm-8 col-lg-6">
                            <textarea type="text" required="" name="category_des" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="form-group row text-right">
                        <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-0">
                            <button type="submit" class="btn btn-space btn-primary">Submit</button>
                            <button class="btn btn-space btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- end valifation types -->
    <!-- ============================================================== -->
</div>
@endsection