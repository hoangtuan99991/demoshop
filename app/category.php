<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class category extends Model
{
    protected $table = 'table_category';

    protected $fillable  = ['category_name', 'category_code', 'category_description'];
    public function getListCategory(){
        $list =  $this->all();
        $ret = [];
        foreach($list as $cat){
            $ret[] = ['id'=>$cat->id, 'name'=>$cat->category_name];
        }
        return $ret;
    }
}
