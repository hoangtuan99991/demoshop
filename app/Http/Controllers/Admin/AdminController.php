<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\product;
use App\category;

class AdminController extends Controller
{
    // //
    // /**
    //  * Create a new controller instance.
    //  * @return void
    //  */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }


    public function index()
    {
        // Test database connection
        try {
            DB::connection()->getPdo();
        } catch (\Exception $e) {
            die("Could not connect to the database.  Please check your configuration. error:" . $e);
        }
        $cat = new category();
        return view('admin.addproduct', ['cat'=> $cat->getListCategory()]);
    }

    /**
     * save product
     */
    public function saveproduct(Request $request)
    {
        $param = $request->all();
        $image = $request->product_image;
        $sub_images = $request->product_sub_image;
        $name = rand(11111, 99999) . '.' . $image->getClientOriginalExtension();
        $uploadPath = public_path('/upload');
        $path = $image->move($uploadPath, $name);

        //upload multil
        $sub_images_arr = [];
        foreach ($sub_images as $key => $sub_image) {
            $name = rand(11111, 99999) . '.' . $image->getClientOriginalExtension();
            $pathsubimage = $sub_image->move($uploadPath, $name);
            $sub_images_arr[] = $pathsubimage->getFilename();
        }
        $product =  product::create([
            'product_name'=> $request['product_name'],
            'product_price'=> $request['product_price'],
            'product_description'=> $request['product_description'],
            'product_code'=> $request['product_code'],
            'product_image'=> $path->getFilename(),
            'product_sub_image'=> implode(',', $sub_images_arr),
            'product_status'=>'1',
            'product_category'=>'1',
        ]);
        return redirect('/admin/addproduct');
    }

    public function addCategory(){
        return view('admin.addcategory');
    }

    public function savecategory(Request $request){
        $param = $request->all();
 
        $product =  category::create([
            'category_name'=> $request['category_name'],
            'category_description'=> $request['category_des'],
            'category_code'=> $request['category_code'],
        ]);
        return redirect('/admin/addcategory');
    }

    public function listProduct(){
        $product = new product();
        $products = [];
        $products = $product->all();
        $products->load('category');
        return view('admin.product', ['products'=> $products]);
    }

    public function deleteProduct(Request $request){
        $params = $request->all();
        $product = new product();
        $p = $product->find($params['id']);
        $p->forceDelete();
        return redirect('/admin/product');
    }
}
