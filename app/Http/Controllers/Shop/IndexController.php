<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\product;
class IndexController extends Controller
{
    // //
    // /**
    //  * Create a new controller instance.
    //  * @return void
    //  */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }


    public function index(){
        // Test database connection
        try {
            DB::connection()->getPdo();
        } catch (\Exception $e) {
            die("Could not connect to the database.  Please check your configuration. error:" . $e );
        }
        return view('shop.index');
    }

    public function product(){
        $products =  product::all();
        return view('shop.product', ['products'=> $products]);
    }

    public function getDetailProduct(Request $request){
        $params = $request->all();
        $product =  product::find($params['id']);
        $product->load('category');
        return view('shop.detailProduct', ['product'=> $product]);
    }

    public function contact(){
        return view('shop.contact');
    }
}
