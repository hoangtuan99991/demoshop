<?php

namespace App;
use category;

use Illuminate\Database\Eloquent\Model;

class product extends Model
{
    protected $table = 'table_product';

    protected $fillable  = ['product_name', 'product_price', 'product_image', 'product_status', 'product_description', 'product_code', 'product_sub_image', 'product_category'];

    public function category(){
        return $this->belongsTo('App\category', 'product_category' , 'id');
    }
}
